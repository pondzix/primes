package com.dixa

import cats.effect.{IO, Resource}
import com.dixa.ProxyConfig.{GrpcClientConfig, HttpServerConfig}
import com.dixa.protocol.PrimesServiceFs2Grpc
import fs2.grpc.syntax.all._
import io.grpc.Metadata
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder
import org.http4s.HttpApp
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.server.Server
import org.http4s.server.middleware.Logger
import pureconfig.ConfigSource

case object ProxyApp

object ProxyFactory {

  def buildApp(configSource: ConfigSource): Resource[IO, ProxyApp.type] = {
    val config = ProxyConfig.load(configSource)

    for {
      grpcChannel <- buildGrpcChannel(config.grpcClient)
      api <- PrimesServiceFs2Grpc.stubResource[IO](grpcChannel)
      httpApp = buildHttpApp(api)
      _ <- buildServer(config.httpServer, httpApp)
    } yield ProxyApp
  }

  private def buildGrpcChannel(grpcClientConfig: GrpcClientConfig) =
    NettyChannelBuilder
      .forAddress(grpcClientConfig.host.toString, grpcClientConfig.port.value)
      .usePlaintext()
      .resource[IO]

  private def buildHttpApp(grpcClient: PrimesServiceFs2Grpc[IO, Metadata]) = {
    val httpApp = Api.primeNumbersRoute(grpcClient).orNotFound
    Logger.httpApp(logHeaders = true, logBody = true)(httpApp)
  }

  private def buildServer(config: HttpServerConfig,
                          httpApp: HttpApp[IO]): Resource[IO, Server] = {
    EmberServerBuilder
      .default[IO]
      .withHost(config.host)
      .withPort(config.port)
      .withHttpApp(httpApp)
      .build
  }
}
