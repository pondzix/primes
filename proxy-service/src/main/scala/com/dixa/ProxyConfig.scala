package com.dixa

import com.comcast.ip4s.{Host, Port}
import ProxyConfig.{GrpcClientConfig, HttpServerConfig}
import pureconfig.generic.auto._
import pureconfig.{ConfigReader, ConfigSource}

final case class ProxyConfig(httpServer: HttpServerConfig,
                             grpcClient: GrpcClientConfig)

object ProxyConfig {

  def load(configSource: ConfigSource): ProxyConfig = configSource.loadOrThrow[ProxyConfig]

  final case class HttpServerConfig(host: Host, port: Port)
  final case class GrpcClientConfig(host: Host, port: Port)

  implicit val hostReader: ConfigReader[Host] = ConfigReader.fromStringOpt(Host.fromString)
  implicit val portReader: ConfigReader[Port] = ConfigReader.fromStringOpt(Port.fromString)
}