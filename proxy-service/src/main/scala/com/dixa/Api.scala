package com.dixa

import cats.effect.IO
import com.dixa.protocol.{PrimeNumbersRequest, PrimesServiceFs2Grpc}
import com.typesafe.scalalogging.LazyLogging
import fs2.{Stream, text}
import io.grpc.Metadata
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl

import scala.util.Try

object Api extends Http4sDsl[IO] with LazyLogging {

  def primeNumbersRoute(grpcPrimeService: PrimesServiceFs2Grpc[IO, Metadata]): HttpRoutes[IO] = HttpRoutes.of {
    case GET -> Root / "prime" / LimitParam(limit) => Ok {
      grpcPrimeService.providePrimesUpToLimit(PrimeNumbersRequest.of(limit.value), new Metadata())
        .map(_.number.toString)
        .handleErrorWith(handleStreamingErrors)
        .intersperse(",")
        .through(text.utf8.encode)
    }
    case GET -> Root / "prime" / invalidParam =>
      BadRequest(s"Passed path parameter: '$invalidParam' is invalid, param must be an integer greater than 1.")
  }

  private def handleStreamingErrors(ex: Throwable): Stream[IO, String] = {
    logger.warn("Received error from rpc client", ex)

    //TODO Provide more detailed errors based on specific throwable (e.g. io.grpc.StatusRuntimeException)
    Stream.emit(s"Internal server error")
  }

  final case class LimitParam(value: Int)

  object LimitParam {
    def unapply(str: String): Option[LimitParam] = {
      Try(str.toInt)
        .filter(_ > 1)
        .map(LimitParam.apply)
        .toOption
    }
  }
}