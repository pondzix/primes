package com.dixa

import cats.effect.{IO, IOApp}
import pureconfig.ConfigSource

object BootProxy extends IOApp.Simple {

  private val configSource = ConfigSource.defaultApplication

  def run: IO[Unit] = {
    ProxyFactory
      .buildApp(configSource)
      .useForever
  }
}
