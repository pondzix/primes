val catsEffectV = "3.2.9"
val http4sV = "0.23.4"
val logbackV = "1.2.5"
val pureConfigV = "0.16.0"
val scalaLoggingV = "3.9.4"
val weaverV = "0.7.6"

lazy val commonSettings = Seq(
  scalaVersion := "2.13.6",
  organization := "com.dixa",
  version := "0.1.0"
)

lazy val root = (project in file("."))
  .settings(commonSettings)
  .settings(name := "prime-numbers")
  .aggregate(proxy, server, protocol, integrationTests)

lazy val proxy = (project in file("proxy-service"))
  .settings(commonSettings)
  .settings(extraJavaProperties("proxy-service"))
  .settings(
    name := "proxy-service",
    libraryDependencies ++= Seq(
      "org.typelevel"               %% "cats-effect"         % catsEffectV,
      "org.http4s"                  %% "http4s-ember-server" % http4sV,
      "org.http4s"                  %% "http4s-circe"        % http4sV,
      "org.http4s"                  %% "http4s-dsl"          % http4sV,
      "ch.qos.logback"              %  "logback-classic"     % logbackV,
      "com.github.pureconfig"       %% "pureconfig"          % pureConfigV,
      "com.typesafe.scala-logging"  %% "scala-logging"       % scalaLoggingV
)
  )
  .dependsOn(protocol)
  .enablePlugins(JavaAppPackaging, DockerPlugin)

lazy val server = (project in file("prime-number-server"))
  .settings(commonSettings)
  .settings(extraJavaProperties("prime-number-server"))
  .settings(
    testFrameworks += new TestFramework("weaver.framework.CatsEffect"),
    name := "prime-number-server",
    libraryDependencies ++= Seq(
      "org.typelevel"               %% "cats-effect"         % catsEffectV,
      "com.github.pureconfig"       %% "pureconfig"          % pureConfigV,
      "ch.qos.logback"              %  "logback-classic"     % logbackV,
      "com.typesafe.scala-logging"  %% "scala-logging"       % scalaLoggingV,
      "com.disneystreaming"         %% "weaver-cats"         % weaverV          % Test,
    )
  )
  .dependsOn(protocol)
  .enablePlugins(JavaAppPackaging, DockerPlugin)

lazy val protocol = (project in file("protocol"))
  .settings(commonSettings)
  .settings(
    name := "protocol",
    libraryDependencies += "io.grpc" % "grpc-netty-shaded" % scalapb.compiler.Version.grpcJavaVersion
  )
  .enablePlugins(Fs2Grpc)

lazy val integrationTests = (project in file("integration-tests"))
  .settings(commonSettings)
  .settings(
    testFrameworks += new TestFramework("weaver.framework.CatsEffect"),
    name := "integration-tests",
    libraryDependencies ++= Seq(
      "org.http4s"                  %% "http4s-blaze-client" % http4sV          % Test,
      "com.disneystreaming"         %% "weaver-cats"         % weaverV          % Test,
    )
  )
  .dependsOn(proxy, server)

def extraJavaProperties(app: String) =
  bashScriptExtraDefines ++= Seq(
    s"""addJava "-Dconfig.file=/etc/$app/application.conf"""",
    s"""addJava "-Dlogback.configurationFile=/etc/$app/logback.xml""""
  )