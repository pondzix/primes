# Prime numbers

## Project structure 

Repository contains:

* `proxy-service` - REST service exposing `GET /prime/<number>` endpoint.
* `prime-number-server` - calculates primes within requested range.
* `protocol` - contains protobuf file (proxy and server depend on this module)
* `integration-tests` - contains integration suites (launching proxy and server, calling proxy REST API) 

## Implementation

Project is written entirely in Scala and mainly relies on following libraries:

* cats effect
* http4s
* fs2 (streams + grpc support)
* weaver (for testing)

## Running apps

### With docker

Just use script `launchApps.sh`, which builds docker images of both apps and then launches them using provided `docker-compose.yml` definition.

### With sbt

To run apps using sbt, one must provide required JVM properties:

* `config.file` - this property specified location of application configuration file
* `logback.configurationFile` - this property specified location of logback configuration file

Run server with sample config:

```shell
sbt -Dconfig.file=./sample-config/for-sbt/server/application.conf -Dlogback.configurationFile=./sample-config/for-sbt/server/logback.xml server/run
```

Run proxy with sample config:

```shell
sbt -Dconfig.file=./sample-config/for-sbt/proxy/application.conf -Dlogback.configurationFile=./sample-config/for-sbt/proxy/logback.xml proxy/run
```

### Sample call

With both applications running, we can try to call proxy API with curl:

```shell
curl -v --no-buffer http://localhost:8085/prime/17
```

which returns in response body `2,3,5,7,11,13,17`.

## Run tests

Run tests for primes server:

```shell
sbt server/test
```

Run integration tests:

```shell
sbt integrationTests/test
```

Run all:

```shell
sbt test
```

## Potential improvements

* Better error handling in stream - translate streaming errors to user-friendly http responses.
* Depending on traffic and maximum allowed upper limit for primes range - optimization of primes calculation like caching, memoization or primes precalculation. Currently primes are calculated using simple sieve of Eratosthenes. 
* Improve configuration of rpc client in proxy (connect timeouts, deadline etc.).