#!/usr/bin/env bash

echo "Building docker images..."
sbt docker:publishLocal

echo "Launching apps with docker-compose..."

docker-compose up -d proxy server