package com.dixa

import cats.effect.{IO, Resource}
import cats.implicits.catsSyntaxApply
import org.http4s.Status
import org.http4s.blaze.client.BlazeClientBuilder
import org.http4s.client.Client
import pureconfig.ConfigSource
import weaver.IOSuite

import scala.concurrent.ExecutionContext.global

object PrimeProxySuite extends IOSuite {

  private val proxyApp = ProxyFactory.buildApp(ConfigSource.resources("application-proxy.conf"))
  private val serverApp = ServerFactory.buildApp(ConfigSource.resources("application-server.conf"))
  private val proxyHttpClient = BlazeClientBuilder[IO](global).resource

  override type Res = ProxyHttpClient
  override val sharedResource: Resource[IO, ProxyHttpClient] = serverApp *> proxyApp *> proxyHttpClient

  type ProxyHttpClient = Client[IO]

  test("Proxy should return primes within provided limit") { proxyClient =>
    proxyClient.expect[String]("http://localhost:8085/prime/17").map { output =>
      expect(output == "2,3,5,7,11,13,17")
    }
  }

  test("Proxy should return 404 not found response when unknown resource is accessed") { proxyClient =>
    proxyClient.get("http://localhost:8085/something") { response =>
      IO(expect(response.status == Status.NotFound))
    }
  }

  test("Proxy should return 400 status when literal as param is passed") { proxyClient =>
    proxyClient.get("http://localhost:8085/prime/something") { response =>
      response.as[String].map { body =>
        expect(response.status == Status.BadRequest) and
          expect(body == "Passed path parameter: 'something' is invalid, param must be an integer greater than 1.")
      }
    }
  }

  test("Proxy should return 400 status when integer lower than 2 as param is passed") { proxyClient =>
    proxyClient.get("http://localhost:8085/prime/1") { response =>
      response.as[String].map { body =>
        expect(response.status == Status.BadRequest) and
          expect(body == "Passed path parameter: '1' is invalid, param must be an integer greater than 1.")
      }
    }
  }
}
