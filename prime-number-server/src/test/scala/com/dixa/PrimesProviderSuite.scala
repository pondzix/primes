package com.dixa

import cats.implicits.toTraverseOps
import com.dixa.PrimesProvider.Limit
import weaver.SimpleIOSuite

object PrimesProviderSuite extends SimpleIOSuite {

  test("Primes provider should return prime list up to given limit (inclusive)") {
    List(
      assert(limit = 2, expectedOutput = List(2)),
      assert(limit = 3, expectedOutput = List(2, 3)),
      assert(limit = 7, expectedOutput = List(2, 3, 5, 7)),
      assert(limit = 20, expectedOutput = List(2, 3, 5, 7, 11, 13, 17, 19))
    ).sequence.map(_.reduceLeft(_ and _))
  }

  private def assert(limit: Int, expectedOutput: List[Int]) = {
    PrimesProvider.findAllPrimesUpTo(Limit(limit).toOption.get).map(_.value).compile.toList.map { output =>
      expect(output == expectedOutput)
    }
  }
}
