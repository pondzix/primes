package com.dixa

import PrimeServerConfig.ServerConfig
import pureconfig.ConfigSource
import pureconfig.generic.auto._

final case class PrimeServerConfig(server: ServerConfig)

object PrimeServerConfig {

  def load(configSource: ConfigSource): PrimeServerConfig = configSource.loadOrThrow[PrimeServerConfig]

  final case class ServerConfig(port: Int)
}