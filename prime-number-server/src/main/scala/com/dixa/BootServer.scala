package com.dixa

import cats.effect.{IO, IOApp}
import pureconfig.ConfigSource

object BootServer extends IOApp.Simple {

  private val configSource = ConfigSource.defaultApplication

  def run: IO[Unit] = {
    ServerFactory
      .buildApp(configSource)
      .useForever
  }
}
