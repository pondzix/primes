package com.dixa

import cats.effect.IO
import fs2.Stream

object PrimesProvider {

  def findAllPrimesUpTo(limit: Limit): Stream[IO, Prime] = {
    def sieve(stream: Stream[IO, Int]): Stream[IO, Int] = {
      stream.head.flatMap { head =>
        val filteredTail = stream.tail.filter(_ % head != 0)
        Stream.emit(head) ++ sieve(filteredTail)
      }
    }

    val initialStream = Stream.range(2, limit.value + 1)
    sieve(initialStream).map(Prime)
  }

  final case class Limit private(value: Int) extends AnyVal
  final case class Prime (value: Int) extends AnyVal

  object Limit {
    def apply(value: Int): Either[Int, Limit] = {
      if (value > 1) Right(new Limit(value))
      else Left(value)
    }
  }

}