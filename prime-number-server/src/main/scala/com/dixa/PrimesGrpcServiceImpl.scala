package com.dixa

import cats.effect.IO
import com.dixa.PrimesProvider.Limit
import com.dixa.protocol.{PrimeNumbersRequest, PrimeNumbersResponse, PrimesServiceFs2Grpc}
import com.typesafe.scalalogging.LazyLogging
import fs2.Stream
import io.grpc.{Metadata, Status, StatusRuntimeException}

object PrimesGrpcServiceImpl
  extends PrimesServiceFs2Grpc[IO, Metadata]
    with LazyLogging {

  override def providePrimesUpToLimit(request: PrimeNumbersRequest,
                                      ctx: Metadata): Stream[IO, PrimeNumbersResponse] = {
    logger.info(s"Received primes request with limit: ${request.limit}")
    Limit(request.limit) match {
      case Right(limit) =>
        handleValidLimit(limit)
      case Left(invalidValue) =>
        handleInvalidLimit(invalidValue)
    }
  }

  private def handleValidLimit(limit: Limit) = {
    PrimesProvider
      .findAllPrimesUpTo(limit)
      .map(prime => PrimeNumbersResponse.of(prime.value))
  }

  private def handleInvalidLimit(invalidValue: Int) = {
    val errorMessage = s"Received invalid limit: '$invalidValue'. Value must be greater than 1."
    logger.warn(errorMessage)
    Stream.raiseError[IO](new StatusRuntimeException(Status.INVALID_ARGUMENT.withDescription(errorMessage)))
  }
}
