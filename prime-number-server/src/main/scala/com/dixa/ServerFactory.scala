package com.dixa

import cats.effect.{IO, Resource}
import com.dixa.protocol.PrimesServiceFs2Grpc
import fs2.grpc.syntax.all._
import io.grpc.ServerServiceDefinition
import io.grpc.netty.shaded.io.grpc.netty.NettyServerBuilder
import pureconfig.ConfigSource

case object ServerApp

object ServerFactory {

  def buildApp(configSource: ConfigSource): Resource[IO, ServerApp.type] = {
    val config = PrimeServerConfig.load(configSource)

    for {
      service <- PrimesServiceFs2Grpc.bindServiceResource[IO](PrimesGrpcServiceImpl)
      _ <- buildGrpcServer(config, service)
    } yield ServerApp
  }

  private def buildGrpcServer(config: PrimeServerConfig,
                              service: ServerServiceDefinition) = {
    NettyServerBuilder
      .forPort(config.server.port)
      .addService(service)
      .resource[IO]
      .evalMap(server => IO(server.start()))
  }
}
